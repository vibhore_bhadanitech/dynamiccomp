import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-student-info',
  templateUrl: './studentinfo.component.html',
  styleUrls: ['./studentinfo.component.css']
})
export class StudentInfoComponent implements OnInit {
  message: any ;
  constructor() { }

  ngOnInit() {
    alert(this.message);
  }
}