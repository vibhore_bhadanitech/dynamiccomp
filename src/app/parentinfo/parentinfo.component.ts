import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-parent-info',
  templateUrl: './parentinfo.component.html',
  styleUrls: ['./parentinfo.component.css']
})
export class ParentInfoComponent implements OnInit {

  message: string | undefined;
  constructor() { }

  ngOnInit() {
    alert(this.message);
  }

}